import React from 'react';
import { render } from '@testing-library/react';
import {
  Task,
  StyledList,
  Heading,
  DesignList,
  ButtonsList,
} from '@components/styled-components/Task';
import 'jest-styled-components';

it('Task renders correctly', () => {
  const { container } = render(<Task />);
  expect(container).toMatchSnapshot();
});

it('StyledList renders correctly', () => {
  const { container } = render(<StyledList />);
  expect(container).toMatchSnapshot();
});

it('Heading renders correctly', () => {
  const { container } = render(<Heading />);
  expect(container).toMatchSnapshot();
});

it('DesignList renders correctly', () => {
  const { container } = render(<DesignList />);
  expect(container).toMatchSnapshot();
});

it('ButtonsList renders correctly', () => {
  const { container } = render(<ButtonsList />);
  expect(container).toMatchSnapshot();
});


