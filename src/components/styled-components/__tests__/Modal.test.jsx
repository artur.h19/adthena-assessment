import React from 'react';
import { render } from '@testing-library/react';
import { screen } from '@testing-library/dom';
import {
  Wrapper,
  Title,
  Icon,
  Button,
  PrimaryButton,
  SecondaryButton,
  Body,
  Footer,
  Loader,
} from '@components/styled-components/Modal';
import 'jest-styled-components';

it('Wrapper renders correctly', () => {
  const { container } = render(<Wrapper />);
  expect(container).toMatchSnapshot();
});

it('Title renders correctly', () => {
  const { container } = render(<Title />);
  expect(container).toMatchSnapshot();
});

it('Icon renders correctly', () => {
  const { container } = render(<Icon />);
  expect(container).toMatchSnapshot();
});

it('Body renders correctly', () => {
  const { container } = render(<Body />);
  expect(container).toMatchSnapshot();
});

it('Footer renders correctly', () => {
  const { container } = render(<Footer />);
  expect(container).toMatchSnapshot();
});

it('Loader renders correctly', () => {
  const { container } = render(<Loader />);
  expect(container).toMatchSnapshot();
});

it('Button renders correctly', () => {
  const { container } = render(<Button />);
  expect(container).toMatchSnapshot();
});

it('PrimaryButton renders correctly', () => {
  const { container } = render(<PrimaryButton />);
  expect(container).toMatchSnapshot();
});

it('PrimaryButton applies correct styles', () => {
  render(<PrimaryButton>Primary button</PrimaryButton>);
  const element = screen.getByText('Primary button')
  expect(element).toHaveStyle(`background: #e8f4ff`)
  expect(element).toHaveStyle(`border: none`)
});

it('SecondaryButton applies correct styles', () => {
  render(<SecondaryButton>Secondary button</SecondaryButton>);
  const element = screen.getByText('Secondary button')
  expect(element).toHaveStyle(`background: #fff`)
  expect(element).toHaveStyle(`border: 2px solid #28639b`)
});

it('Wrapper applies correct styles', () => {
  render(<Wrapper data-testid="wrapper" />);
  const element = screen.getByTestId('wrapper')
  expect(element).toHaveStyle(`max-height: 350px;`)
});

it('Wrapper applies correct styles when isLoading', () => {
  render(<Wrapper isLoading data-testid="wrapper" />);
  const element = screen.getByTestId('wrapper')
  expect(element).toHaveStyle(`max-height: 200px`)
});