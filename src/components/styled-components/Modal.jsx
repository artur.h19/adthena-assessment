import styled from 'styled-components';

export const Wrapper = styled.div`
  min-height: 210px;
  max-width: 550px;
  border: 2px solid #d51c34;
  padding: 20px 25px;
  border-radius: 12px;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
  position: fixed;
  z-index: 123;
  top: 0;
  bottom: 0;
  right: 0;
  left: 0;
  margin: auto;
  background: #fff;
  height: auto;
  max-height: 350px;

  ${(props) => {
    if (props.size === 'small') {
      return `
        max-height: 200px;
      `;
    }

    if (props.size === 'big') {
      return `
        max-height: 350px;
    `;
    }

    return null;
  }}

  ${(props) => {
    if (props.isLoading) {
      return `
        justify-content: center;
        align-items: center;
        max-height: 200px;
      `;
    }

    return null;
  }}
`;

export const Title = styled.h3`
  font-family: 'Roboto', sans-serif;
  font-weight: 900;
  margin: 0 0 22px 0;
  border-bottom: 1px solid #e4e4e4;
  line-height: 1.5rem;
  text-align: left;
`;

export const Icon = styled.img`
  width: 20px;
  margin: 0 9px -4px 0;
`;

export const Button = styled.button`
  font-family: 'Roboto', sans-serif;
  font-weight: 500;  
  font-size: 1rem;
  color: #28639b;
  padding: 11px 25px;
  border-radius: 5px;
  min-width: 115px;
  outline: none;
  cursor: pointer;
  border: none;

  &:hover {
    opacity: 0.7;
  };
`;

export const PrimaryButton = styled(Button)`
  background: #e8f4ff;
`;

export const SecondaryButton = styled(Button)`
  border: 2px solid #28639b;
  background: #fff;
`;

export const Body = styled.div`
  min-height: 50px;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  text-align: left;
`;

export const Footer = styled.div`
  font-family: 'Roboto', sans-serif;
  font-weight: 300;    
  display: flex;
  justify-content: flex-end;
  margin-top: auto;

  & > button:first-child {
    margin-right: 11px;
  }
`;

export const Loader = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;  

  img {
    width: 75px;
  };

  span {
    margin-top: 10px;
    font-family: 'Roboto', sans-serif;
    font-weight: 500;
    color: #28639b;
    font-size: 1.1rem;
  }
`;
