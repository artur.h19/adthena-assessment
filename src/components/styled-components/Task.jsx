import styled from 'styled-components';

export const Task = styled.div`
  display: flex;
  flex-direction: column;
  width: 800px;
  margin: auto;
`;

export const Content = styled.div`
  text-align: justify;
`;

export const StyledList = styled.ol`
  text-align: justify;
  
  li {
    line-height: 2;
  }
`;

export const Heading = styled.h1`
  color: #216298;
`;

export const DesignList = styled.div`
  display: flex;
  flex-direction: column;

  > div {
    display: flex;
    flex-direction: column;
  }

  img {
    width: 80%;
    margin: 0 auto;
  }

  button {
    max-width: 200px;
    margin: 10px auto;
  }
`;

export const ButtonsList = styled.div`
  display: flex;
  flex-direction: column;
  
  button {
    max-width: 200px;
    margin: 15px;
  };
`;

export const TextArea = styled.textarea`
  width: calc(100% - 18px);
  margin: 40px 0 10px 0;
  color: #28639b;
  padding: 5px 9px;
`;

export const Input = styled.input`
  max-width: 100px;
  margin: 5px 0;
`;
