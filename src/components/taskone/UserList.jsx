import React, { useState, useCallback } from 'react';

import debounce from '@utils/debounce';
import useFetch from '@utils/useFetch';
import * as style from '@components/styled-components/UserList';

import { API_CALL_DELAY } from './constants';

const UserList = () => {
  const [value, setValue] = useState('');
  const [filter, setFilter] = useState('');

  const { response, error, isLoading } = useFetch(`/users${filter ? `?username=${encodeURIComponent(filter)}` : ''}`);

  const debounceSetFilter = useCallback(
    debounce((event) => {
      setFilter(event.target.value);
    }, API_CALL_DELAY), []
  );

  const onFilterChange = (e) => {
    setValue(e.target.value);

    debounceSetFilter(e);
  };

  return (
    <div data-testid="user-data">
      <div>
        Filter:
        <input
          type="text"
          onChange={onFilterChange}
          value={value}
          placeholder="Enter username"
          data-testid="username"
        />
      </div>
      {error ? 'Something went wrong' : (
        <style.Users>
          {/* TODO: add Loader here */}
          {isLoading ? 'Loading' : (
            Array.isArray(response) && response.map((user) => (
              <style.Row key={user.id}>
                <style.UserInfo>
                  <span>{`Name: ${user.name}`}</span>
                  <span>{`Username: ${user.username}`}</span>
                </style.UserInfo>
                <div>
                  <div>
                    <span>{user.address.street}</span>
                    <span>{user.address.suite}</span>
                    <span>{user.address.city}</span>
                    <span>{user.address.zipcode}</span>
                  </div>
                  <div>
                    <span>{user.email}</span>
                    <span>{user.phone}</span>
                  </div>
                </div>
              </style.Row>
            ))
          )}
        </style.Users>
      )}
    </div>
  );
};

export default UserList;
