import React from 'react';
import { render, fireEvent, cleanup } from '@testing-library/react';
import { renderHook } from '@testing-library/react-hooks';
import useFetch from '../../../utils/useFetch';
import UserList from '../UserList';
import { usersMockData } from '../users.mock';

afterEach(cleanup)

function setupFetchStub() {
  return function fetchStub(_url) {
    return new Promise((resolve) => {
      resolve({
        json: () =>
          Promise.resolve({
            data: usersMockData,
          }),
      })
    })
  }
};

global.fetch = jest.fn().mockImplementation(setupFetchStub())

beforeAll(() => {
  global.fetch = jest.fn().mockImplementation(setupFetchStub())
});

afterAll(() => {
  global.fetch.mockClear()
}) ;

it('Text in state is changed when user inputs', () => {
    const { getByTestId } = render(<UserList />);

    expect(getByTestId("username").value).toBe('');
    
    fireEvent.change(getByTestId("username"), { target: { value: 'Bret' }});

    expect(getByTestId("username").value).toBe('Bret');
});

// TODO: watch open issue that's connected with act(() => {}) warning
test('Test useFetch', async () => {
  const { result, waitForNextUpdate } = renderHook(
    () => useFetch(),
  );
  
  expect(result.current.response).toEqual(null);
  expect(result.current.isLoading).toEqual(true);
  expect(result.current.error).toEqual(null);

  await waitForNextUpdate();

  expect(result.current.response.data).toEqual(usersMockData);
  expect(result.current.isLoading).toEqual(false);
  expect(result.current.error).toEqual(null);
});
