import React, { useState } from 'react';

import {
  Content,
  DesignList,
  Heading,
  StyledList,
  Task,
  ButtonsList,
  TextArea,
  Input,
} from '@components/styled-components/Task';
import { PrimaryButton } from '@components/styled-components/Modal';

import Modal from './Modal';

const modals = [
  {
    title: 'Data loading modal: ',
    alt: 'Data loading modal',
    src: 'images/data_loading.png'
  },
  {
    title: 'Delete files modal: ',
    alt: 'Delete files modal',
    src: 'images/delete_files.png'
  },
  {
    title: 'Delete report and history modal: ',
    alt: 'Delete report and history modal',
    src: 'images/delete_report_and_history.png'
  },
];

const TaskTwo = () => {
  const [isLoadingModalOpen, setIsLoadingModalOpen] = useState(false);
  const [isDeleteFilesModalOpen, setIsDeleteFilesModalOpen] = useState(false);
  const [isHistoryModalOpen, setIsHistoryModalOpen] = useState(false);

  const onDeleteFilesMock = () => {
    console.log('Delete files');
  };

  return (
    <Task>
      <Heading>Task Two</Heading>
      <Content>
        <h4>Complete the following task:</h4>
        <p>
          This is to assess your ability to design and build React components.
          Assume that you are part of a team starting a new project,
          a consistent design language has been set by the Design Team,
          i.e. fonts, labels, button and input styles etc. Below are
          designs for some components for you to build. With unit tests
          and without 3rd party modal packages, build the modals shown in
          these designs and have re-usability in mind as what you build
          might be re-usable in other parts of the application.
        </p>
        <p>
          {`Include unit tests. React Testing Library is already 
            configured for you in the skeleton project.`}
        </p>
        Expected:
        <StyledList type="1">
          <li>
            <strong>No </strong>
            backdrop/page overlay is required for this exercise.
          </li>
          <li>All modals should expose a mechanism to allow them to be closed.</li>
          <li>There should be a way to consume button click events on modals with buttons.</li>
          <li>Create a demo page with three buttons that trigger each of the modals to show.</li>
        </StyledList>
        <DesignList>
          {modals.map(({ title, alt, src }, index) => (
            <div key={index.toString()}>
              <label htmlFor={`img-${index}`}>
                {title}
              </label>
              <img
                alt={alt}
                src={src}
                id={`img-${index}`}
              />
            </div>
          ))}
        </DesignList>
        <hr />
        <strong>
          Feel free to use this component as a demo page for your work.
        </strong>
      </Content>

      <Modal
        close={() => setIsLoadingModalOpen(false)}
        isOpen={isLoadingModalOpen}
        isLoading
      />

      <Modal
        close={() => setIsDeleteFilesModalOpen(false)}
        isOpen={isDeleteFilesModalOpen}
        title="Are you sure you want to delete all of your files?"
      >
        The action cannot be undone
      </Modal>

      <Modal
        close={() => setIsHistoryModalOpen(false)}
        isOpen={isHistoryModalOpen}
        confirmBtnText="Delete all"
        rejectBtnText="Cancel"
        action={onDeleteFilesMock}
        size="big"
        title="Are you sure you want to delete this report and its history?"
      >
        If you delete the Exeutive metrics report,
        you will also delete the associated history:
        <TextArea rows={5} />

        Please type the word &ldquo;Delete&rdquo; to remove
        the Executive metrics report and its associated history:
        <Input />
      </Modal>

      <ButtonsList>
        <PrimaryButton
          type="button"
          onClick={() => setIsLoadingModalOpen(true)}
        >
          Open Data loading modal
        </PrimaryButton>
        <PrimaryButton
          type="button"
          onClick={() => setIsDeleteFilesModalOpen(true)}
        >
          Open Delete files modal
        </PrimaryButton>
        <PrimaryButton
          type="button"
          onClick={() => setIsHistoryModalOpen(true)}
        >
          Open Delete report and history modal
        </PrimaryButton>
      </ButtonsList>
    </Task>
  );
};

export default TaskTwo;
