import React, { useEffect, useState, useRef } from 'react';
import PropTypes from 'prop-types';

import useOnClickOutside from '@utils/useOnClickOutside';
import DangerIcon from '@assets/warning-sign.svg';
import Loader from '@assets/loader.svg';

import * as style from '@components/styled-components/Modal';

const Modal = ({
  isOpen,
  children,
  title,
  close,
  isLoading,
  action,
  confirmBtnText,
  rejectBtnText,
  size,
}) => {
  const [open, setOpen] = useState(isOpen);
  const ref = useRef();

  useEffect(() => {
    setOpen(isOpen);
  }, [isOpen]);

  const onClose = () => {
    setOpen(false);
    close();
  };

  const onAccept = () => {
    action();
    onClose();
  };

  useOnClickOutside(ref, onClose);

  return !open ? null
    : (

      <style.Wrapper ref={ref} size={size} isLoading={isLoading}>
        {
        isLoading ? (
          <style.Loader>
            <img src={Loader} alt="loader" />
            <span>Data is loading</span>
          </style.Loader>
        ) : (
          <>
            <style.Title>
              <style.Icon src={DangerIcon} alt="danger-icon" />
              {title}
            </style.Title>
            {children && (
              <style.Body>
                {children}
              </style.Body>
            )}
            <style.Footer>
              <style.PrimaryButton onClick={onAccept}>{confirmBtnText}</style.PrimaryButton>
              <style.SecondaryButton onClick={onClose}>{rejectBtnText}</style.SecondaryButton>
            </style.Footer>
          </>
        )
       }
      </style.Wrapper>
    );
};

Modal.defaultProps = {
  isOpen: false,
  children: null,
  close: () => {},
  action: () => {},
  isLoading: false,
  confirmBtnText: 'Yes',
  rejectBtnText: 'No',
  title: 'Modal Title',
  size: 'small',
};

Modal.propTypes = {
  isOpen: PropTypes.bool,
  children: PropTypes.node,
  close: PropTypes.func,
  isLoading: PropTypes.bool,
  action: PropTypes.func,
  confirmBtnText: PropTypes.string,
  rejectBtnText: PropTypes.string,
  title: PropTypes.string,
  size: PropTypes.string,
};

export default Modal;
