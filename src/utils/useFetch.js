import { useEffect, useState } from 'react';

const useFetch = (url) => {
  const [response, setResponse] = useState(null);
  const [error, setError] = useState(null);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    setIsLoading(true);
    const fetchData = async () => {
      try {
        const res = await fetch(`${process.env.API_BASE}${url}`);

        if (res.status && res.status !== 200) {
          throw new Error('Something went wrong. Check API_BASE');
        }

        const json = await res.json();
        setResponse(json);
        setIsLoading(false);
      } catch (e) {
        setError(e);
        setIsLoading(false);
      }
    };
    fetchData();
  }, [url]);

  return { response, error, isLoading };
};

export default useFetch;
