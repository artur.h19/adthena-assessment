function debounce(fn, delay) {
  let timeoutId;

  return function f(...args) {
    const context = this;
    if (timeoutId) {
      clearTimeout(timeoutId);
    }

    timeoutId = setTimeout(() => {
      fn.apply(context, args);
    }, delay);
  };
}

export default debounce;
