const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractCssChunks = require('extract-css-chunks-webpack-plugin');
const Dotenv = require('dotenv-webpack');

const environment = process.env.NODE_ENV;
const isDevelopment = environment === 'development';

const resolvePath = (relativePath) => path.resolve(__dirname, relativePath);

const PATHS = {
  public: resolvePath('public'),
  dist: resolvePath('dist'),
  components: resolvePath('src/components'),
  services: resolvePath('src/services'),
  utils: resolvePath('src/utils'),
  assets: resolvePath('src/assets'),
};

module.exports = {
  entry: './src/index.jsx',
  mode: environment,
  resolve: {
    extensions: ['.js', '.jsx', '.json'],
    alias: {
      '@components': PATHS.components,
      '@services': PATHS.services,
      '@utils': PATHS.utils,
      '@assets': PATHS.assets,
    },
  },
  output: {
    filename: 'main.js',
    path: PATHS.dist,
  },
  module: {
    rules: [
      {
        test: /\.(jsx|js|es6)$/,
        exclude: /node_modules/,
        loader: 'eslint-loader',
        enforce: 'pre',
        options: {
          cache: false
        }
      },
      {
        test: /\.(jsx|js|es6)$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        options: { presets: ['@babel/env'] },
      },
      {
        test: /\.(sa|sc|c)ss$/,
        use: [
          {
            loader: ExtractCssChunks.loader,
            options: {
              hot: isDevelopment,
            },
          },
          {
            loader: 'css-loader',
            options: {
              sourceMap: isDevelopment,
            },
          },
          {
            loader: 'postcss-loader',
            options: {
              sourceMap: isDevelopment,
            },
          },
        ],
      },
      {
        test: /\.(html)$/,
        use: [
          {
            loader: 'raw-loader',
          },
        ],
        exclude: [/public/],
      },
      {
        test: /\.(png|jpg|gif|svg)$/i,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 8192,
            },
          },
        ],
      },
    ],
  },
  devtool: isDevelopment ? 'inline-source-map' : 'source-map',
  plugins: [
    new ExtractCssChunks({
      filename: '[name].css',
      chunkFilename: '[id].css',
    }),
    new HtmlWebpackPlugin({
      hash: true,
      template: './public/index.html',
      filename: 'index.html',
      favicon: './public/favicon.ico',
      env: {
        environment,
      },
      chunksSortMode: 'none',
    }),
    new Dotenv({
      path: './.env', // load this now instead of the ones in '.env'
      safe: true, // load '.env.example' to verify the '.env' variables are all set. Can also be a string to a different file.
      allowEmptyValues: true, // allow empty variables (e.g. `FOO=`) (treat it as empty string, rather than missing)
      systemvars: true, // load all the predefined 'process.env' variables which will trump anything local per dotenv specs.
      silent: true,
    }),
  ],
  devServer: {
    contentBase: PATHS.public,
    historyApiFallback: true,
    host: '127.0.0.1',
    port: 8082,
  },
};
